package com.fcoffee.bk.fcoffee;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.fcoffee.bk.fcoffee.Model.Customer;

public class PersonActivity extends AppCompatActivity {
        //boolean Click=false;
        private Customer customer = null;
        @Override
        protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);
        getData();
       // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_person);
        //setSupportActionBar(toolbar);

            if (customer !=null) {
                TextView name = (TextView) findViewById(R.id.name);
                TextView email = (TextView) findViewById(R.id.email);
                name.setText(customer.getUsename());
                email.setText(customer.getMail());
            }

        addOnClickButton();
    }

    public void addOnClickButton() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.list_content_person);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                // Handle navigation view item clicks here.
                int id = item.getItemId();

                if (id == R.id.info_person) {
                    View viewListContent = (View) findViewById(R.id.list_content_person);
                    ///add action on click
                    viewListContent.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(PersonActivity.this,Popup_info.class);

                    Bundle bundle = new Bundle();
                    bundle.putSerializable("account", customer);
                    intent.putExtra("data", bundle);
                    intent.putExtra("type",1);
                    startActivity(intent);
                    viewListContent.setVisibility(View.VISIBLE);
                } else if (id == R.id.list_order) {

                } else if (id == R.id.list_place_add) {

                } else if (id == R.id.list_checkin) {

                }

                //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
               // drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });

        Button back = (Button) findViewById(R.id.back_person);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              finish();
            }
        });

    }
            @Override
            public void onBackPressed() {
                finish();
                //  Disable Button Back
            }
    public void getData(){
        Bundle bundle = getIntent().getBundleExtra("data");
        if (bundle != null){
            customer = (Customer) bundle.getSerializable("account");
        }

    }

}