package com.fcoffee.bk.fcoffee;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.fcoffee.bk.fcoffee.Model.Customer;

/**
 * Created by anhri on 4/25/2017.
 */

public class Popup_info extends Activity {
    private Customer customer = null;
    private Customer[] user = new Customer[100];

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        Bundle type = intent.getExtras();
        getData();

        if (type!=null) {
            switch (type.getInt("type")) {
                case 1:
                    if (customer!= null) {
                        setContentView(R.layout.popup_info);
                        ListViewInfo(customer);
                    }
                    break;
                case 2:
                    setContentView(R.layout.popup_info);
                    break;
                case 3:
                    setContentView(R.layout.popup_info);
                    break;
                case 4:
                    setContentView(R.layout.popup_info);
                    break;
                default:
                    break;
            }
        }

        DisplayMetrics infoPopup = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(infoPopup);

        int width = infoPopup.widthPixels;
        int height = infoPopup.heightPixels;

        getWindow().setWindowAnimations(R.style.Animation2);
        getWindow().setLayout(width,height-55);
    }
/*
    public  Customer SetUserInfo(int id, String name, String email, String phone, String password, int type){
        Customer user = new Customer(id,name,email,phone,password,type);
        return user;
    }
*/
    @Override
    public void onBackPressed() {
        finish();
        //  Disable Button Back
    }

    public void ListViewInfo(Customer userdata){
        ListView listView = (ListView) findViewById(R.id.listview_info);

        ListItem itemUserName = new ListItem("User Name",userdata.getUsename());
        ListItem itemEmail = new ListItem("Email", userdata.getMail());
        ListItem itemPhone = new ListItem("Phone",userdata.getPhone());
        ListItem itemPass = new ListItem("Password",userdata.getPassword().replaceAll(userdata.getPassword(),"******"));

        ListItem listItem[] = new ListItem[]{itemUserName,itemEmail,itemPhone,itemPass};

        ArrayAdapter<ListItem> arrayAdapter = new ArrayAdapter<ListItem>(this,android.R.layout.simple_expandable_list_item_1,listItem);
        listView.setAdapter(arrayAdapter);
    }

    public class ListItem{
        private String title;
        private String content;

        public ListItem(String title, String content){
            this.title = title;
            this.content = content;
        }

        public void setTitle(String title){
            this.title = title;
        }
        public  void setContent(String content){
            this.content = content;
        }

        public String getTitle(){
            return title;
        }

        public String getContent(){
            return content;
        }

        @Override
        public String toString(){
            return (this.title+"\n"+this.content);
        }
    }
    public void getData(){
        Bundle bundle = getIntent().getBundleExtra("data");
        if (bundle != null){
            customer = (Customer) bundle.getSerializable("account");
        }

    }
}
