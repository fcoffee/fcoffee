package com.fcoffee.bk.fcoffee;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.fcoffee.bk.fcoffee.ListView.TestStore;

import java.util.ArrayList;

public class ShopInfoActivity extends AppCompatActivity {
    ArrayList<TestStore> arr = null;
    private int position;
    public String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_info);
        // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(toolbar);

        getData();
        if (arr != null) {
            TextView shopname = (TextView) findViewById(R.id.shop_name);
            shopname.setText(arr.get(position).getName().toString());
            TextView shopadd = (TextView) findViewById(R.id.shop_add);
            shopadd.setText(arr.get(position).getAddress().toString());
            ImageView shopcover = (ImageView) findViewById(R.id.shop_cover);
            String uri_icon = "drawable/" + arr.get(position).getIcon();
            int ImageResoure = shopcover.getContext().getResources().getIdentifier(uri_icon, null, shopcover.getContext().getApplicationContext().getPackageName());
            Drawable image = shopcover.getContext().getResources().getDrawable(ImageResoure);
            shopcover.setImageDrawable(image);
        }
        addOnClick();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.chiduong);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String add = "google.navigation:q=" + Double.toString(arr.get(position).getv())+','+Double.toString(arr.get(position).getk());
                Uri gmmIntentUri = Uri.parse(add);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setupRatingBar();
    }

    private void addOnClick() {
        Button call = (Button) findViewById(R.id.call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(ShopInfoActivity.this);
                builder.setCancelable(false);
                builder .setMessage(arr.get(position).getSdt());
                builder.setPositiveButton(R.string.dialog_call, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + arr.get(position).getSdt()));
                        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        if (ActivityCompat.checkSelfPermission(ShopInfoActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(callIntent);
                    }
                })
                        .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Action for "Cancel".
                                dialog.cancel();
                            }
                        });
                AlertDialog dialog = builder.show();
                //dialog.setTitle(R.string.dialog_call_title);

                TextView myMsg = (TextView) dialog.findViewById(android.R.id.message);
                myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
            }
        });
    }

    private void getData() {
        Bundle packageFromCaller = getIntent().getBundleExtra("data");
        position =  packageFromCaller.getInt("position");
        arr = new ArrayList<TestStore>();
        arr = (ArrayList<TestStore>) packageFromCaller.getSerializable("listStore");
    }

    private void setupRatingBar() {
        RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.rgb(144,99,192), PorterDuff.Mode.SRC_ATOP);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                TextView numRating = (TextView) findViewById(R.id.numRate);
                switch ((int) ratingBar.getRating()) {
                    case 1:
                        numRating.setText("1.0");
                        break;
                    case 2:
                        numRating.setText("2.0");
                        break;
                    case 3:
                        numRating.setText("3.0");
                        break;
                    case 4:
                        numRating.setText("4.0");
                        break;
                    case 5:
                        numRating.setText("5.0");
                        break;
                    default:
                        numRating.setText("0.0");
                        break;
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
