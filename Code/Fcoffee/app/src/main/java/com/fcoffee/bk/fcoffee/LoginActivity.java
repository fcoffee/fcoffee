package com.fcoffee.bk.fcoffee;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.fcoffee.bk.fcoffee.Constant.ConstantDictionary;
import com.fcoffee.bk.fcoffee.DatabaseManager.Dictionary;
import com.fcoffee.bk.fcoffee.DatabaseManager.IData;
import com.fcoffee.bk.fcoffee.DatabaseManager.MyDatabaseHelper;
import com.fcoffee.bk.fcoffee.Model.Customer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Random;

public class LoginActivity extends AppCompatActivity {
    private Dictionary dictionary;
    private Customer customer =null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        checkDatabase();
        dictionary = new Dictionary(getApplicationContext(),"",null,1);
        ChoiseBackground();
        LoginButton();
        SupportButton();
    }

    private void ChoiseBackground() {
        View bg = (View) findViewById(R.id.login_page);
        Random rand = new Random();
        switch (rand.nextInt(3) + 1){
            case 1:
                bg.setBackgroundResource(R.drawable.bglogin);
                break;
            case 2:
                bg.setBackgroundResource(R.drawable.bglogin1);
                break;
            case 3:
                bg.setBackgroundResource(R.drawable.bglogin2);
                break;
            default:
                bg.setBackgroundResource(R.drawable.bglogin);
                break;
        }
    }

    //Void Login Button
    private void LoginButton(){

        ImageButton FacebookButton = (ImageButton) findViewById(R.id.btn_fblogin);
        ImageButton GoogleButton = (ImageButton) findViewById(R.id.btn_gglogin);
        TextView textView = (TextView) findViewById(R.id.textView1);
        textView.setVisibility(View.INVISIBLE);

        Button LoginButton = (Button) findViewById(R.id.btn_login);

        //Login button
        LoginButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //action click on Login Button
                EditText user_name = (EditText) findViewById(R.id.user_name);
                EditText password = (EditText) findViewById(R.id.password);
                //Get user name and password
                String user = user_name.getText().toString();
                String pass = password.getText().toString();
                if (isCorrect(user, pass)) {
                    OpenActivityMain();
                } else {
                    password.setText("");
                    Toast.makeText(getApplicationContext(), "Username or password is incorrect!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Facebook button
        FacebookButton.setVisibility(View.INVISIBLE);
        FacebookButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //action click on Facebook login button
            }
        });
        //Google button
        GoogleButton.setVisibility(View.INVISIBLE);
        GoogleButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void onClick(View v){
                //action click on Google login button
            }
        });

    }
    //Tempt
    private boolean isCorrect(String user, String pass) {
        ArrayList<IData>arr = dictionary.SearchData(ConstantDictionary.TABLE_CUSTOMER, ConstantDictionary.CHECK_LOGIN, user, pass);
        if(arr.size()==0) return  false;
        else {
            customer = (Customer) arr.get(0);
            return true;
        }
    }

    //Forget Pass and Creat New Account
    private void SupportButton(){
        TextView ForgetPass = (TextView) findViewById(R.id.forget_pass);
        TextView CreateNewAcc = (TextView) findViewById(R.id.new_acc);


        ForgetPass.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                // action something
            }
        });
        CreateNewAcc.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                // action something
            }
        });
    }

    //Next Activity When Login Success
    public void OpenActivityMain(){
        Intent intent = new Intent(this,MainActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("account", customer);
        intent.putExtra("data", bundle);
        startActivity(intent);
        finish();
    }
    public void checkDatabase() {
        File data = getDatabasePath(MyDatabaseHelper.PATHNAME);
        dictionary = new Dictionary(getApplicationContext(),"",null,1);
        if(data.exists() == false || true){
            copyDatabase(getApplicationContext());
        }
    }
    public void copyDatabase(Context context){
        try{
            InputStream inputStream =
                    context.getAssets().open(MyDatabaseHelper.PATHNAME);
            String outputString = MyDatabaseHelper.PATH +
                    MyDatabaseHelper.PATHNAME;
            dictionary.getReadableDatabase();
            dictionary.getWritableDatabase();
            OutputStream outputStream = new
                    FileOutputStream(outputString);
            byte[]arr = new byte[10240];
            int length =0;
            while ((length =inputStream.read(arr))>0){
                outputStream.write(arr,0,length);
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}