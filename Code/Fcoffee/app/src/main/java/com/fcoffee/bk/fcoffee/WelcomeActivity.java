package com.fcoffee.bk.fcoffee;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import static android.os.Build.VERSION_CODES.M;

public class WelcomeActivity extends AppCompatActivity
{
    private final int SPLASH_DISPLAY_LENGTH = 3000;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_welcome);

        if(!checkInternetConnection()){
            openDialogCheckConnect();
        }
        else {
            if(!checkGPS()){
                openDialogCheckGPS();
            } else {
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                        Intent mainIntent = new Intent(WelcomeActivity.this,LoginActivity.class);
                        WelcomeActivity.this.startActivity(mainIntent);
                        WelcomeActivity.this.finish();
                    }
                }, SPLASH_DISPLAY_LENGTH);
            }
        }
    }

    private boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        // test for connection
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean checkGPS(){
        LocationManager mLocationManager;
        final int MY_PERMISSON_FINE_LOCATION = 101;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
           // mMap.setMyLocationEnabled(true);
        } else {
            if (Build.VERSION.SDK_INT >= M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSON_FINE_LOCATION);
            }
        }
        //Get Current Location
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean mcheckGPS = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return mcheckGPS;
    }
    public void openDialogCheckConnect() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle(R.string.dialog_connect_title);
        dialog.setMessage(R.string.dialog_connect_content);
        dialog.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                dialog.cancel();
                finish();
            }
            })
                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Action for "Cancel".
                        dialog.cancel();
                        finish();
                    }
                });

        final AlertDialog alert = dialog.create();
        alert.show();
    }
    public void openDialogCheckGPS() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle(R.string.dialog_gps_title);
        dialog.setMessage(R.string.dialog_gps_content);
        dialog.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                dialog.cancel();
                finish();
            }
        })
                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Action for "Cancel".
                        dialog.cancel();
                        finish();
                    }
                });

        final AlertDialog alert = dialog.create();
        alert.show();
    }

}
