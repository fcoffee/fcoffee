package com.fcoffee.bk.fcoffee;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fcoffee.bk.fcoffee.ListView.TestStore;
import com.fcoffee.bk.fcoffee.Model.Customer;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import pl.bclogic.pulsator4droid.library.PulsatorLayout;

import static android.os.Build.VERSION_CODES.M;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;


public class MainActivity extends AppCompatActivity
        implements OnMapReadyCallback {
    private Customer customer = null;
    private GoogleMap mMap;
    private View mapView;
    ArrayList<TestStore> arr = null;
    ArrayList<TestStore> arr2 = null;
    public double x, y;
    public boolean numscan = false;
    private LocationManager mLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Show Tool Bar
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(toolbar);

        getData();
        setData();

        // Show and action Nav Bar
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                // Handle navigation view item clicks here.
                int id = item.getItemId();

                if (id == R.id.nav_map) {
                    //Do nothing
                } else if (id == R.id.nav_person) {
                    OpenNextActivity(id);
                } else if (id == R.id.nav_like) {
                    OpenNextActivity(id);
                } else if (id == R.id.nav_suggest) {
                    OpenNextActivity(id);
                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                return true;
            }
        });

        View view = navigationView.getHeaderView(0);
        TextView user = (TextView) view.findViewById(R.id.user_name);
        if (customer != null) {
            user.setText(customer.getUsename());
        }

        // Get the SupportMapFragment and request notification when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();

        AddClickAction();


        FloatingActionButton bold = (FloatingActionButton) findViewById(R.id.scan);
        bold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScanEffect();
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                        numscan = scanLocationNearMe(x, y, mMap);
                        PulsatorLayout pulsator = (PulsatorLayout) findViewById(R.id.pulsator);
                        pulsator.stop();
                        if (numscan) {
                            Intent intent = new Intent(MainActivity.this, PoplistShop.class);

                            Bundle bundle = new Bundle();
                            bundle.putSerializable("listStore", arr2);
                            intent.putExtra("data", bundle);

                            startActivity(intent);
                        } else {
                            Toast.makeText(MainActivity.this, "Không tìm thấy địa điểm nào gần bạn", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, 7000);
            }
        });

    }

    private void setData() {
        arr = new ArrayList<TestStore>();
        TestStore store0 = new TestStore("The Coffee House", "249, Lý Thường Kiệt, Q.11, Tp. Hồ Chí Minh", 10.771845, 106.657675, "tch", "0989897405");
        arr.add(store0);
        TestStore store1 = new TestStore("Ny Coffee", "249, Lý Thường Kiệt, Q.11, Tp. Hồ Chí Minh", 10.770978, 106.657885, "nc", "0989897405");
        arr.add(store1);
        TestStore store2 = new TestStore("Cafe Đăng Khoa", "243, Lý Thường Kiệt, Q.11, Tp. Hồ Chí Minh", 10.771534, 106.657824, "cdk", "0989897405");
        arr.add(store2);
        TestStore store3 = new TestStore("Arlon Coffee", "233, Lý Thường Kiệt, Q.11, Tp. Hồ Chí Minh", 10.771055, 106.657855, "alc", "0989897405");
        arr.add(store3);
        TestStore store4 = new TestStore("Cafe Gia Phố", "11, Đường số 2, Q.11, Tp. Hồ Chí Minh", 10.772145, 106.656858, "cgp", "0989897405");
        arr.add(store4);
        TestStore store5 = new TestStore("Milano Coffee", "6, Đường số 2, Q.11, Tp. Hồ Chí Minh", 10.772237, 106.657018, "mc", "0989897405");
        arr.add(store5);
        TestStore store6 = new TestStore("Cà Phê Góc Phố", "52, Lữ Gia, Q.11, Tp. Hồ Chí Minh", 10.770887, 106.654392, "cpgp", "0989897405");
        arr.add(store6);
        TestStore store7 = new TestStore("Rovina Coffee", "2B, Đường số 2, Q.11, Tp. Hồ Chí Minh", 10.772408, 106.657336, "rc", "0989897405");
        arr.add(store7);
        TestStore store8 = new TestStore("Red Tea House", "523, Tô Hiến Thành, Q.10, Tp. Hồ Chí Minh", 10.772949, 106.661088, "rth", "0989897405");
        arr.add(store8);
        TestStore store9 = new TestStore("Cafe Ngọc Yên", "354/30, Lý Thường Kiệt, Q.10, Tp. Hồ Chí Minh", 10.775719, 106.658088, "cny", "0989897405");
        arr.add(store9);
    }

    private void ScanEffect() {
        PulsatorLayout pulsator = (PulsatorLayout) findViewById(R.id.pulsator);
        pulsator.start();
    }

    public void getData() {
        Bundle bundle = getIntent().getBundleExtra("data");
        if (bundle != null) {
            customer = (Customer) bundle.getSerializable("account");
        }
    }

    /////// GOOGLE MAP SETUP /// BEGIN ./
    private final static int MY_PERMISSON_FINE_LOCATION = 101;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //Cap quyen truy cap GPS
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            if (Build.VERSION.SDK_INT >= M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSON_FINE_LOCATION);
            }
        }
        /// Set Vi tri button my location
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 20, 20);
        }
        myLocation();
    }

    private void myLocation() {

        //Cap quyen truy cap GPS
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            if (Build.VERSION.SDK_INT >= M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSON_FINE_LOCATION);
            }
        }

        //Get Current Location
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (statusOfGPS) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, mLocationListener);
            Location location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            x = location.getLatitude();
            y = location.getLongitude();

            LatLng mLocation = new LatLng(x, y);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLocation, 15));

        } else {
            //onPause();
        }
    }

    //Cap quyen truy cap GPS
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSON_FINE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        mMap.setMyLocationEnabled(true);
                        mMap.getUiSettings().setMyLocationButtonEnabled(true);
                        mMap.getUiSettings().setZoomControlsEnabled(true);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Vui lòng cấp quyền truy cập GPS cho ứng dụng", Toast.LENGTH_LONG).show();
                    //onPause();
                    finish();
                }
                break;
        }
    }
    /////// GOOGLE MAP SETUP /// END ./

    /// CLICK ON BUTTON
    private void AddClickAction() {
        TextView logout = (TextView) findViewById(R.id.btn_logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //action on click
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        Button showNav = (Button) findViewById(R.id.menu_main);
        showNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.openDrawer(GravityCompat.START);
            }
        });
    }

    /// ONCLICK NAV BAR
    public void OpenNextActivity(int id) {
        if (id == R.id.nav_map) {
            // do nothing
        } else if (id == R.id.nav_like) {
            Intent intent = new Intent(this, PlaceLikeActivity.class);

            Bundle bundle = new Bundle();
            bundle.putSerializable("account", customer);
            intent.putExtra("data", bundle);

            startActivity(intent);
        } else if (id == R.id.nav_person) {
            Intent intent = new Intent(this, PersonActivity.class);

            Bundle bundle = new Bundle();
            bundle.putSerializable("account", customer);
            intent.putExtra("data", bundle);

            startActivity(intent);
        } else if (id == R.id.nav_suggest) {
            Intent intent = new Intent(this, SuggestActivity.class);

            Bundle bundle = new Bundle();
            bundle.putSerializable("account", customer);
            intent.putExtra("data", bundle);

            startActivity(intent);
        }
        ;
        // finish();
    }

    @Override
    public void onBackPressed() {
        //  Disable Button Back
        // super.onBackPressed();
        // super.onPause();
    }

    private double distance(double v, double k,double vi, double ki){
        double dv = v-vi;
        double dk = k-ki;

        double R = 6371;

        double a = (sin( dk/2 ) * sin( dk/2 ))  + cos( k ) * cos( ki ) * (sin( dv/2 )*sin( dv/2 ));
        double c = 2 * atan2( sqrt( a ), sqrt( 1-a ) );
        double d = R * c;

        return d * 3.1412 / 180;
    }

    private Boolean scanLocationNearMe(double v, double k, GoogleMap googleMap) {
        int count = 0;
        arr2 = new ArrayList<TestStore>();
        for (int i = 0 ; i<arr.size();i++){
            if (distance(v,k,arr.get(i).getv(),arr.get(i).getk())<=1) {
                count++;
                LatLng tch = new LatLng(arr.get(i).getv(), arr.get(i).getk());
                googleMap.addMarker(new MarkerOptions().position(tch)
                        .title(arr.get(i).getName())
                        .snippet("8:00 - 22:00 (h)")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
                arr2.add(arr.get(i));
            }
        }
        if (count!=0) return true;
        return false;
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
            x = location.getLatitude();
            y = location.getLongitude();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

}
