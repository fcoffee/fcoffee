package com.fcoffee.bk.fcoffee;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fcoffee.bk.fcoffee.ListView.MyArrayAdapter;
import com.fcoffee.bk.fcoffee.ListView.TestStore;

import java.util.ArrayList;

/**
 * Created by anhri on 4/22/2017.
 */

public class PoplistShop extends Activity {

   // private Store store[] = null;
    ArrayList<TestStore> arr = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_listshop);

        DisplayMetrics listShop = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(listShop);

        int width = listShop.widthPixels;
        int height = listShop.heightPixels;

        getWindow().setWindowAnimations(R.style.Animation);
        getWindow().setLayout(width,height-55);

        getData();

        ListView lv = (ListView) findViewById(R.id.list_shop);
        MyArrayAdapter myArr = new MyArrayAdapter(this, R.layout.list_row, arr);
        lv.setAdapter(myArr);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(PoplistShop.this,ShopInfoActivity.class);

                Bundle bundle = new Bundle();
                bundle.putSerializable("listStore", arr);
                bundle.putInt("position", position);
                intent.putExtra("data", bundle);

                startActivity(intent);
                finish();
            }
        });
    }
/*
    private float LITMIT_MOVE_TOUCH = 50;
    public float y1,y2;

    @Override
    public boolean dispatchTouchEvent(MotionEvent event){

        switch(event.getActionMasked()) {
            case (MotionEvent.ACTION_DOWN):
               // y1 = event.getRawY();
                return true;
            case (MotionEvent.ACTION_MOVE):
               // y2 = event.getRawY();
                return true;
            case (MotionEvent.ACTION_UP):

                float delta = Math.abs(y1-y2);
                if (delta > LITMIT_MOVE_TOUCH){
                    finish();
                }

                return true;
            default :
                return super.onTouchEvent(event);
        }
    }
*/
    public void getData(){
        Bundle packageFromCaller = getIntent().getBundleExtra("data");
        arr = new ArrayList<TestStore>();
        arr = (ArrayList<TestStore>) packageFromCaller.getSerializable("listStore");
    }

}
