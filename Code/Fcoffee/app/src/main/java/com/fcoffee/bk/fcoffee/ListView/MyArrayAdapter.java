package com.fcoffee.bk.fcoffee.ListView;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fcoffee.bk.fcoffee.R;

import java.util.ArrayList;

/**
 * Created by DELL on 5/5/2017.
 */

public class MyArrayAdapter extends ArrayAdapter {
    Activity context = null;
    ArrayList<TestStore> myArray = null;
    int layoutId;
    public MyArrayAdapter(Activity context, int layoutId, ArrayList<TestStore> arr){
        super(context, layoutId, arr);
        this.context = context;
        this.layoutId = layoutId;
        this.myArray = arr;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(layoutId, null);
        }
        // Lấy ra những control được đinh nghĩa trong cấu trúc
        TestStore store = myArray.get(position);
        ImageView icon = (ImageView) convertView.findViewById(R.id.list_image);
        TextView nameStore = (TextView) convertView.findViewById(R.id.name_store);
        TextView addressStore = (TextView) convertView.findViewById(R.id.address_store);

        nameStore.setText(store.getName());
        addressStore.setText(store.getAddress());
        // TODO
        //icon.setImageResource(R.drawable.pcloudy);
        String uri_icon = "drawable/" + store.getIcon();
        int ImageResoure = convertView.getContext().getResources().getIdentifier(uri_icon, null, convertView.getContext().getApplicationContext().getPackageName());
        Drawable image = convertView.getContext().getResources().getDrawable(ImageResoure);
        icon.setImageDrawable(image);
        return convertView;
    }
}
