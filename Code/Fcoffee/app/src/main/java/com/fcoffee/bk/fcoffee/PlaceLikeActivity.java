package com.fcoffee.bk.fcoffee;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.fcoffee.bk.fcoffee.Model.Customer;
import com.fcoffee.bk.fcoffee.Model.Store;

public class PlaceLikeActivity extends AppCompatActivity
{
    private Customer customer = null;
    private Store store = null;
    private double vido,kinhdo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_like);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_place);
        setSupportActionBar(toolbar);

        getData();

        vido = 10.772176;
        kinhdo = 106.657660;

        store = new Store(0,"THE COFFEE HOUSE","268 Lý Thường Kiệt, phường 6, quận 10",null,3.5f,null,1);
        setContentView(store);

        addOnClickButton();
    }
    public void setContentView(Store mStore){
        //Image Preview
        ImageView temp = (ImageView) findViewById(R.id.imageShop);
        temp.setImageResource(R.drawable.imageview); // Replace Image of Shop

        //Shop Name
        TextView textview = (TextView) findViewById(R.id.shopTitle);
        textview.setText(mStore.getName());

        //Vote Rating
        RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingBar.setRating(mStore.getRate());

        //Dia chi Shop
        TextView addShop = (TextView) findViewById(R.id.shop_add);
        addShop.setText(mStore.getAddress());
    }

    public void addOnClickButton() {
        Button showNav = (Button) findViewById(R.id.back_place);
        showNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              finish();
            }
        });

        final Button chidan = (Button) findViewById(R.id.chidan);
        chidan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String add = "google.navigation:q=" +vido+','+kinhdo;
                Uri gmmIntentUri = Uri.parse(add);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
            }
        });

        Button move_left = (Button) findViewById(R.id.btn_left);
        move_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //  setContentShop(list_Shop[1],"The Coffee 1",(float)3.5);
            }
        });

        Button move_right = (Button) findViewById(R.id.btn_right);
        move_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   setContentShop(list_Shop[3],"The Coffee 3",(float)5);
            }
        });
    }

    public void onBackPressed() {
        finish();
    }
    public void getData(){
        Bundle bundle = getIntent().getBundleExtra("data");
        if (bundle != null){
            customer = (Customer) bundle.getSerializable("account");
        }

    }
}