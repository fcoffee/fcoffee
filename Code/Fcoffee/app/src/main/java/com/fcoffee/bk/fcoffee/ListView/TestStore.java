package com.fcoffee.bk.fcoffee.ListView;

/**
 * Created by DELL on 5/6/2017.
 */

import java.io.Serializable;

/**
 * Created by DELL on 5/4/2017.
 */

public class TestStore implements Serializable {
    private String name;
    private String address;
    private Double v;
    private Double k;
    private String icon;
    private String sdt;
    public TestStore(String name, String address, double v, double k , String icon, String std) {
        this.name = name;
        this.address = address;
        this.v = v;
        this.k = k;
        this.icon = icon;
        this.sdt = std;
    }
    public String getName() {
        return name;
    }
    public String getAddress() {
        return address;
    }
    public Double getv() {
        return v;
    }
    public Double getk() {
        return k;
    }
    public String getIcon() {
        return icon;
    }
    public String getSdt() {return sdt;}

}
