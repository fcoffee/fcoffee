package com.fcoffee.bk.fcoffee.Model;


import com.fcoffee.bk.fcoffee.DatabaseManager.IData;

/**
 * Created by dainguyen on 4/17/17.
 */

public class Store implements IData {
    private int id;
    private String name;
    private String address;
    private String time;
    private float rate;
    private String price;
    private int type;

    public Store(int id, String name, String address, String time, float rate, String price, int type) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.time = time;
        this.rate = rate;
        this.price = price;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}

